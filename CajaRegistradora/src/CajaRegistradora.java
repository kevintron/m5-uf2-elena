import java.util.Scanner;

/**
 * @author Kevin Alcolea Caceres
 * @see https://files.merca20.com/uploads/2018/02/TIENDITA.jpg
 * @since 9/04/2021
 * @version 1.0
 * @category IES_SABADELL
 */

public class CajaRegistradora {

	/**
	 * M�tode director de la classe.
	 * @param args 
	 */
    public static void main(String[] args) {

    	
    	/**
         * Inicialitzem les variables de tot el programa, primer.
         */
    	
    	/**
         * @param E2 Monedes de 2�.
         */
    	int E2=0;
       
    	/**
    	 * @param E1 Monedes de 1�.
    	 */
        int E1=1;
        
        /**
         * @param C50 Monedes de 0.5�.
         */
        int C50=2;
        
        /**
         * @param C20 Monedes de 0.2�.
         */
        int C20=3;
        
        /**
         * @param C10 Monedes de 0.1�.
         */
        int C10=4;
        
        /**
         * @param C5 Monedes de 0.05�
         */
        int C5=5;
        
        /**
         * @param C2 Monedes de 0.02�.
         */
        int C2=6;
        
        /**
         * @param C1 Monedes de 0.01�.
         */
        int C1=7;
        
        /**
         * @param dinero Diners a retornar al client.
         */
        int[] dinero;
        
        /**
         * @param faltaDar Dinero que falta por dar al cliente.
         */
        
        int faltaDar=0;
        
        /**
         * @param total Total improte de la compra.
         */
        
        int total=0;
        
        /**
         * @dado Importe dado por el cliente.
         */
        
        int dado=0;
        
        
    	
        
        
        
        /**L'utilitzarem per a introduir l'import de la compra i els diners donats pel client.
    	 * @param Scanner Inicialitzaci� de l'esc�ner per teclat.
    	 */
    		Scanner sc = new Scanner(System.in);
    	
    		System.out.println("Introdueix el cost de la compra, en c�ntims: ");
            total=sc.nextInt();
            
            System.out.println("Introdueix la quantitat donada pel client, en c�ntims: ");
            dado=sc.nextInt();

            dinero = new int[8];
            sc.close();
  
        	
            
            /**
             * Comen�a el proc�s 
             */
           if (total > dado) {
            	System.out.println(" ");
            	System.out.println("******************************");
                System.out.println("FALTA PER PAGAR " + (total - dado));
            	System.out.println("******************************");
            	System.out.println(" ");

            }
            else {

                faltaDar = dado - total;
                while (faltaDar > 0) {

                    if (faltaDar >= 200) {
                        faltaDar -= 200;
                        dinero[E2]++;
                    }
                    else if (faltaDar >= 100) {
                        faltaDar -= 100;
                        dinero[E1]++;
                    }
                    else if (faltaDar >= 50) {
                        faltaDar -= 50;
                        dinero[C50]++;
                    }
                    else if (faltaDar >= 20) {
                        faltaDar -= 20;
                        dinero[C20]++;
                    }
                    else if (faltaDar >= 10) {
                        faltaDar -= 10;
                        dinero[C10]++;
                    }
                    else if (faltaDar >= 5) {
                        faltaDar -= 5;
                        dinero[C5]++;
                    }
                    else if (faltaDar >= 2) {
                        faltaDar -= 2;
                        dinero[C2]++;
                    }
                    else if (faltaDar >= 1) {
                        faltaDar -= 1;
                        dinero[C1]++;
                    }

                }

                System.out.println("Li has donat al client: ");
                for (int x = 0; x < 8; x++) {
                    System.out.print(dinero[x]);
                    if (x < 7) System.out.print(" ");
                }

                System.out.println("");
            }

        

    }
}
